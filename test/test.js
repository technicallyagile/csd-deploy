var expect = require('chai').expect;
var calculateString = require('../index.js');

describe('addTwoNumbers()', function () {

  it ('should return itself when it is just a number', function() {
    var equation = "5";
    let result = calculateString(equation);
    expect(result).to.be.equal(5);
  });

  it ('should return itself when it is just a number 2', function() {
    var equation = "4";
    let result = calculateString(equation);
    expect(result).to.be.equal(4);
  });

  it ('should add two numbers', function () {
    var equation = "4+2";
    let result = calculateString(equation);
    expect(result).to.be.equal(6);
  });

});
