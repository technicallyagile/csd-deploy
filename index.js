function calculateString(equation) {
   if (equation.includes('+')) {
     let numbers  = equation.split('+');
     let result  =  Number(numbers[0]) + Number(numbers[1]);
     return result;
   }
   return Number(equation);
}

module.exports = calculateString;
